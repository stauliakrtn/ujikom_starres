<?php
include "head.php";
?>
<?php
include'../database.php';
$db = new database();
?>
<div class="breadcrumbs">
  <div class="col-sm-4">
    <div class="page-header float-left">
      <div class="page-title">
        <h1>Dashboard</h1>
      </div>
    </div>
  </div>
  <div class="col-sm-8">
    <div class="page-header float-right">
      <div class="page-title">
        <ol class="breadcrumb text-right">
          <li><a href="#">Dashboard</a></li>
          <li><a href="#">Table</a></li>
          <li class="active">Data table</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<div class="content mt-3">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <strong class="card-title">Table Data Orderan</strong>
          </div>
          <div class="card-body">
            <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>No</th>
                  <th>No Meja</th>
                  <th>Nama User</th>
                  <th>Tanggal</th>
                  <th>Keterangan</th>
                  <th>Status Order</th>
                  <th>Keterangan Pembayaran</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php
              error_reporting(0);
              $no = 1;
              foreach($db->tampil_data_order() as $x){
                ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $x['no_meja']; ?></td>
                  <td><?php echo $x['tanggal']; ?></td>
                  <td><?php echo $x['nama_user']; ?></td>
                  <td><?php echo $x['keterangan']; ?></td>
                  <td><?php
                                            if($x['status_order'] == 'Y')
                                            {
                                              ?>
                                            <?php echo "Sudah di Konfirmasi";?>
                                          
                                            <?php
                                            }else{
                                              ?>
                                          <?php echo "Belum di Konfirmasi";?>
                                            <?php 
                                            }
                                            ?></td>                  
                    <td>
                    <?php
                  if($x['keterangan_transaksi'] == 'Y')
                  {
                    ?>

                    <?php echo "Sudah Dibayar";?>

                    <?php
                  }else{
                    ?>
                    <?php echo "Belum Dibayar";?>
                    <?php 
                  }
                  ?></td>
                  <td>
                                            <?php if ($x['status_order'] == 'N'){ ?>
                                              <a href="keterangan_update.php?id_order=<?php echo $x['id_order'];?>" class="btn btn-success btn-md">Konfirmasi</a> <?php }?>
                                              <a href="detail_order_w.php?id_order=<?php echo $x['id_order'];?>" class="btn btn-danger">Detail</a>    
                                           </td>
                   
                    </tr>
                    <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
include "foot.php";
?>
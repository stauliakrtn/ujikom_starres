
<?php
include "head.php";
?>
<?php
include'../database.php';
$db = new database();
?>
<?php require_once("../koneksi.php");
    
    if (!isset($_SESSION)) {
        session_start();
    } ?>
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Dashboard</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="#">Table</a></li>
                    <li class="active">Data table</li>
                </ol>
            </div>
        </div>
    </div>
</div>
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        
                        <div class="card-body">
                            <?php
                          include'../koneksi.php';
                                            // Include / load file koneksi.php
                                            // Cek apakah terdapat data pada page URL
                          $page = (isset($_GET['page'])) ? $_GET['page'] : 1;

                          $limit = 4; // Jumlah data per halamanya

                                            // Buat query untuk menampilkan daa ke berapa yang akan ditampilkan pada tabel yang ada di database
                          $limit_start = ($page - 1) * $limit;

                                            // Buat query untuk menampilkan data siswa sesuai limit yang ditentukan
                          $data=mysqli_query($conn, "SELECT * FROM masakan where status_makanan='Y'LIMIT ".$limit_start.",".$limit);
                          $no = $limit_start + 1; // Untuk penomoran tabel
                          while($show=mysqli_fetch_array($data)){
                        ?>
                        <div class="col-lg-3 col-md-4">
                            <div class="card">
                                <div class="card-body">
                                    <div class="mx-auto d-block">
                                        <img class="rounded-squere mx-auto d-block" src="../images/<?php echo $show['gambar']; ?>" alt="Card image cap">
                                        <h5 class="text-sm-center mt-2 mb-1"><?php echo $show['nama_masakan'];?></h5>
                                        <div class="location text-sm-center">Harga : Rp. <?php echo$show['harga'];?></div>
                                    </div>
                                    <hr>
                                    <div class="card-text text-sm-center">
                                        <a href="cart.php?act=add&amp;id_masakan=<?php echo $show['id_masakan']; ?> &amp;ref=entri_order.php" class="btn btn-primary btn-block"> Pesan</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                        
                            <div class="pagination">
                            <?php
                            if ($page == 1) { // Jika page adalah pake ke 1, maka disable link PREV
                            ?>
                                <li class="disabled"><a href="#">First</a></li>
                                <li class="disabled"><a href="#">&laquo;</a></li>
                            <?php
                            } else { // Jika buka page ke 1
                                $link_prev = ($page > 1) ? $page - 1 : 1;
                            ?>
                                <li><a href="entri_order.php?page=1">First</a></li>
                                <li><a href="entri_order.php?page=<?php echo $link_prev; ?>">&laquo;</a></li>
                            <?php
                            }
                            ?>

                            <!-- LINK NUMBER -->
                            <?php
                            // Buat query untuk menghitung semua jumlah data
                            $sql2 = mysqli_query($conn,"SELECT COUNT(*) AS jumlah FROM masakan where status_makanan='Y' ");
                            ($get_jumlah = (mysqli_fetch_array($sql2)));

                            $jumlah_page = ceil($get_jumlah['jumlah'] / $limit); // Hitung jumlah halamanya
                            $jumlah_number = 3; // Tentukan jumlah link number sebelum dan sesudah page yang aktif
                            $start_number = ($page > $jumlah_number) ? $page - $jumlah_number : 1; // Untuk awal link member
                            $end_number = ($page < ($jumlah_page - $jumlah_number)) ? $page + $jumlah_number : $jumlah_page; // Untuk akhir link number

                            for ($i = $start_number; $i <= $end_number; $i++) {
                                $link_active = ($page == $i) ? 'class="active"' : '';
                            ?>
                                <li <?php echo $link_active; ?>><a href="entri_order.php?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
                            <?php
                            }
                            ?>

                            <!-- LINK NEXT AND LAST -->
                            <?php
                            // Jika page sama dengan jumlah page, maka disable link NEXT nya
                            // Artinya page tersebut adalah page terakhir
                            if ($page == $jumlah_page) { // Jika page terakhir
                            ?>
                                <li class="disabled"><a href="#">&raquo;</a></li>
                                <li class="disabled"><a href="#">Last</a></li>
                            <?php
                            } else { // Jika bukan page terakhir
                                $link_next = ($page < $jumlah_page) ? $page + 1 : $jumlah_page;
                            ?>
                                <li><a href="entri_order.php?page=<?php echo $link_next; ?>">&raquo;</a></li>
                                <li><a href="entri_order.php?page=<?php echo $jumlah_page; ?>">Last</a></li>
                            <?php
                            }
                            ?>
                            </div>
                        <form action="proses_pesan.php" method="post">
                            <div class="card-body">
                                <table id="" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <?php
                                                if (isset($_SESSION['items1'])) {
                                                    foreach ($_SESSION['items1'] as $key => $val) {
                                                        $query = mysqli_query($conn, "SELECT * FROM meja WHERE id_meja = '$key'");
                                                        $data = mysqli_fetch_array($query);
                                            ?>
                                                <input type="hidden" name="no_meja" value="<?php echo $data['id_meja']?>;">
                                            <?php
                                                    }
                                                }
                                            ?>
                                            <th>No</th>
                                            <th>Nama Masakan</th>
                                            <th>Harga</th>
                                            <th>Quantity</th>
                                            <th>Sub Total</th>
                                            <th>Keterangan</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                  <tbody>
                                   <?php
                                    //MENAMPILKAN DETAIL KERANJANG BELANJA//
                                      $no = 1;
                                      $total = 0;
                                    //mysql_select_db($database_conn, $conn);
                                      if (isset($_SESSION['items'])) {
                                          foreach ($_SESSION['items'] as $key => $val) {
                                            $query = mysqli_query($conn, "SELECT * FROM masakan WHERE id_masakan = '$key'");
                                            $data = mysqli_fetch_array($query);
                                            $jumlah_barang = mysqli_num_rows($query);
                                            $jumlah_harga = $data['harga'] * $val;
                                            $total += $jumlah_harga;
                                            $harga = $data['harga'];
                                            $hasil = "Rp.".number_format($harga,2,',','.');
                                            $hasil1 = "Rp.".number_format($jumlah_harga,2,',','.');
                                            ?>
                                            <tr>
                                                <td><?php echo $no++; ?></td>
                                                <td><input type="hidden" name="id_masakan[]" value="<?php echo $data['id_masakan']; ?>"><?php echo $data['nama_masakan']; ?></td>
                                                <td><?php echo $hasil; ?></td>
                                                <td><a href="cart.php?act=plus&amp;id_masakan=<?php echo $data['id_masakan']; ?> &amp;ref=entri_order.php" class="btn btn-default"><i class="ti-plus"></i></a><input type="hidden" name="jumlah[]" value="<?php echo ($val); ?>"><?php echo ($val); ?> Pcs <a href="cart.php?act=min&amp;id_masakan=<?php echo $data['id_masakan']; ?> &amp;ref=entri_order.php" class="btn btn-default"><i class="ti-minus"></i></a>
                                                </td>
                                                <td><?php echo $hasil1; ?></td>
                                                <td><textarea class="form-control" name="keterangan[]"></textarea></td>
                                                <td><a href="cart.php?act=del&amp;id_masakan=<?php echo $data['id_masakan']; ?> &amp;ref=entri_order.php">Cancel</a></td>
                                            </tr>
                                            <?php } } ?>                                      
                                            <?php
                                                if($total == 0){ ?>
                                                    <td colspan="4" align="center"><?php echo "Tabel Makanan Anda Kosong!"; ?></td>
                                                <?php } else { ?>
                                                <td colspan="6" style="font-size: 18px;"><b><div style="padding-right: 80px;" class="pull-right"><input type="hidden" value="<?php echo $total;?>" name="total_bayar">Total Harga Anda : Rp. <?php echo number_format($total); ?>,- </div> </b></td>
          
                                            <?php 
                                            }
                                            ?>
                                  </tbody>
                                </table>
                                <p><div style="padding-right: 75px;" align="right">
                                    <button type="submit"  class="btn btn-success">&raquo; Konfirmasi Pemesanan &laquo;</button>
                                    </div>
                                </p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
include "foot.php";
?>